﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicNew
{
    class MagicPyramid
    {
        static byte[,,] mas =
            {
            { {26,19,18,17}, {25,16,24,23}, {31,29,30,34}    },
            { {0,11,1,2},    {10,8,9,19},   {35,34,33,32}    },
            { {20,13,12,11}, {21,23,22,16}, {27,32,28,29}    },
            { {4,2,3,13} ,   {5,14,15,16},  {6,17,7,8}       }
        };   //Массив вершин граней, которые меняются местами при операции поворота грани, чтобы не менять местами элементы основания, которых больше 
        public char[] Color { get; private set; }   //Массив цветов граней
        public int[] ColorOfMagic { get; private set; }  //номера которые нужны для Magic repaint

        public MagicPyramid(MagicPyramid obj)  //Обязательно потом убрать это
        {
            Color = new char[36];
            ColorOfMagic = new int[36];
            for (int i = 0; i < 36; Color[i] = obj.Color[i], ColorOfMagic[i] = obj.ColorOfMagic[i], i++) { }
        }
        public MagicPyramid(char[] mas, int[] color_repaint)
        {
            Color = mas;
            ColorOfMagic = color_repaint;
        }

        public void CopyPyramid(MagicPyramid obj)   //не нашел удобного способа копирования обьекта, наследовать от ICloneable дольше
        { //должно пригодиться при переборе комбинаций
            Color = new char[36];
            ColorOfMagic = new int[36];
            for (int i = 0; i < 36; Color[i] = obj.Color[i], ColorOfMagic[i] = obj.ColorOfMagic[i], i++) { }

        }

        public void Rotate(byte NameFace, bool clockwise)
        {
            char[,] R = new char[3, 4];
            for (byte i = 0; i < 4; i++)
            {
                R[0, i] = Color[mas[NameFace, 0, i]];
                R[1, i] = Color[mas[NameFace, 1, i]];
                R[2, i] = Color[mas[NameFace, 2, i]];
            }
            if (clockwise)
            {
                for (byte i = 0; i < 4; i++)
                {
                    Color[mas[NameFace, 0, i]] = R[1, i];
                    Color[mas[NameFace, 1, i]] = R[2, i];
                    Color[mas[NameFace, 2, i]] = R[0, i];
                }
            }
            else
            {
                for (byte i = 0; i < 4; i++)
                {
                    Color[mas[NameFace, 1, i]] = R[0, i];
                    Color[mas[NameFace, 2, i]] = R[1, i];
                    Color[mas[NameFace, 0, i]] = R[2, i];
                }
            }
        }

        public void Show()  //По сути просто кривой вывод элементов в консоль, делать более красивый нет смысла, на задачу не влияет
        {
            int k = 11;
            int j = 0;
            string t = "";
            for (int i = 0; i < 6; i++)
            {
                Console.Write(t);
                for (int n = j + k; j < n; j++)
                {

                    Console.Write(String.Format("{0,3}", Color[j]));
                }
                Console.WriteLine();
                k -= 2;
                t += "  ";
            }
        }

        public void Magic_repaint()
        {
            char[] color_old = new char[36];
            for (int i = 0; i < 36; color_old[i] = Color[i], i++) { }

            for (int i = 0; i < 36; i++)
            {
                Color[i] = color_old[ColorOfMagic[i] - 1];
            }
        }

        public bool Done()  //Проверка на собранность
        {
            int[] x = { 1, 2, 3, 4, 5, 12, 13, 14, 21 }; //проверяю одинаковые ли числа в 2 гранях, этого дожно быть достаточно
            int[] u = { 6, 15, 16, 17, 22, 23, 24, 25, 26 };
            for (int i = 0; i < 9; i++)
            {
                if (Color[x[1]] != Color[x[i] - 1]) return false;
                if (Color[u[1]] != Color[u[i] - 1]) return false;
            }

            return true;
        }
    }

    delegate void ts(int i);
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            MagicPyramid Pyramid = new MagicPyramid("ddddcccccabbbbbdcaabbbddacccaabddaaa".ToArray<char>(), a);
            


            //Усложнение примера выше, +3 действия, всего 5
            Pyramid.Rotate(2, true);
            Pyramid.Rotate(0, true);
            Pyramid.Rotate(1, false);



            Stopwatch time = new Stopwatch();  //таймер для рассчета скорости
            time.Start();

           
            //Очень плохой перебор, но как вышло
            //добавить проверку собрана ли изначально
            for (int i = 0; i < 10; i++)
                for (int i1 = 0; i1 < 10; i1++)
                    for (int i2 = 0; i2 < 10; i2++)
                        for (int i3 = 0; i3 < 10; i3++)
                            for (int i4 = 1; i4 < 10; i4++)
                            {
                                MagicPyramid p2 = new MagicPyramid(Pyramid);
                                Bbrut(i4,p2);
                                Bbrut(i3,p2);
                                Bbrut(i2,p2);
                                Bbrut(i1,p2);
                                Bbrut(i,p2);
                                if (p2.Done())
                                {
                                    time.Stop();
                                    Console.WriteLine("Подобрано: {0}{1}{2}{3}{4}\nЗа {5} ms", i4, i3, i2, i1, i, time.ElapsedMilliseconds);
                                    return;
                                }

                            }

            Console.ReadKey();
        }


        static void Bbrut(int index, MagicPyramid pyramid)
        {
            switch (index)
            {
                case 0:
                    return;
                case 1:
                    pyramid.Rotate(0, true);
                    break;
                case 2:
                    pyramid.Rotate(1, true);
                    break;
                case 3:
                    pyramid.Rotate(2, true);
                    break;
                case 4:
                    pyramid.Rotate(3, true);
                    break;
                case 5:
                    pyramid.Rotate(0, false);
                    break;
                case 6:
                    pyramid.Rotate(1, false);
                    break;
                case 7:
                    pyramid.Rotate(2, false);
                    break;
                case 8:
                    pyramid.Rotate(3, false);
                    break;
                case 9:
                    pyramid.Magic_repaint();
                    break;
            }
        }
    }
}
